from appgestion.models import Solicitud_Contacto
from django.shortcuts import render
from django.http import HttpResponse


def eliminar_solicitud_por_id(request):
    if request.GET["txt_id"]: #si el txt_id viene con un dato if se ejecuta
        id_recibido=request.GET["txt_id"]
        solicitud_contacto=Solicitud_Contacto.objects.filter(id=id_recibido)
        if solicitud_contacto:
            sol_con=Solicitud_Contacto.objects.get(id=id_recibido)
            sol_con.delete()
            mensaje="Solicitud de contacto eliminada"
        else:
            mensaje="Solicitud no eliminada. No existe una solicitud con ese ID" 
    else:
        mensaje= "Debe ingresar un id"
    return HttpResponse(mensaje)