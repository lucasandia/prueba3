from django.shortcuts import render
from django.http import HttpResponse
from appgestion.models import Casa
from appgestion.models import Solicitud_Contacto

# Create your views here.

def contacto(request):
    return render(request,"contacto.html")

def portada(request):
    return render(request,"portada.html")

def quienes_somos(request):
    return render(request,"quienes_somos.html")    

def galeria(request):
    return render(request,"galeria.html")

def buscar_sol_contacto(request):
    return render(request,"buscar_solicitudes_contacto.html")    

def formulario_eliminar(request):
    return render(request,"eliminar_solicitud_contacto.html")

def  ingresar_solicitud(request):
    nombre=request.GET["txt_nombre"]
    correo=request.GET["txt_correo"]
    comuna=request.GET["opt_comuna"]
    telefono=request.GET["txt_telefono"]
    mensaje=request.GET["txta_mensaje"]
    if len(nombre)>0 and len(correo)>0 and len(comuna)>0 and len(telefono) >0 and len(mensaje) >0 :
        Sol= Solicitud_Contacto(nombre=nombre,correo=correo,comuna=comuna,telefono=telefono,mensaje=mensaje)
        Sol.save()
        return render(request,"ingreso_solicitud_correcta.html")
    else:
        return render(request,"ingreso_solicitud_erronea.html")         

def buscar_por_cant_habitaciones(request):
    if request.GET['txt_c_habitaciones']: 
        cantidad_recibida=request.GET['txt_c_habitaciones']
        casas=Casa.objects.filter(cantidad_habitaciones__icontains=cantidad_recibida) 
        #__gte es > o =
        #__lte <=
        return render(request,"resultados_cant_habitaciones.html",{"casas":casas,"cantidad_consultada":cantidad_recibida})
    else:
        mensaje="Debe ingresar una cantidad  para buscar"
    return HttpResponse(mensaje)

def buscar_sol_por_email(request):
    if request.GET['txt_correo']:
        email_recibido=request.GET['txt_correo']
        solicitud_contactos=Solicitud_Contacto.objects.filter(correo__icontains=email_recibido)
        return render(request,"resultados_busqueda_por_email.html",{"solicitud_contactos":solicitud_contactos,"email_consultado":email_recibido})
    else:
        mensaje="Debe ingresar un email para buscar"
    return HttpResponse(mensaje) 

def buscar_sol_por_comuna(request):
    if request.GET['txt_comuna']:
        comuna_recibida=request.GET['txt_comuna']
        solicitud_contactos=Solicitud_Contacto.objects.filter(comuna__icontains=comuna_recibida)
        return render(request,"resultados_busqueda_por_comuna.html",{"solicitud_contactos":solicitud_contactos,"comuna_consultada":comuna_recibida})
    else:
        mensaje="Debe ingresar una comuna para buscar"
    return HttpResponse(mensaje) 
    