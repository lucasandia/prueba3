class casa_a_medida(object):
    def __init__(self,precio_banio,precio_habitacion,numero_banios,numero_habitaciones):
        self.precio_banio=precio_banio
        self.precio_habitacion=precio_habitacion
        self.numero_banios=numero_banios
        self.numero_habitaciones=numero_habitaciones

    def get_precio_banio(self):
        return self.precio_banio

    def get_precio_habitacion(self):
        return self.precio_habitacion


    def get_numero_banios(self):
        if self.numero_banios<3:
            return self.numero_banios
       # else:
        #    return self.numero_banios*0    

    def get_numero_habitaciones(self):
        if self.numero_habitaciones<5:
            return self.numero_habitaciones
       # else:
        #    return self.numero_habitaciones*0    

    def precio_final(self):
        if self.numero_banios<3  and self.numero_habitaciones<5:
            return(self.precio_banio*self.numero_banios)+(self.precio_habitacion*self.numero_habitaciones)
        else:
            return(self.precio_banio*self.numero_banios*0)+(self.precio_habitacion*self.numero_habitaciones*0)

from django.http import HttpResponse
from django.template.loader import get_template      

def funcion_casa_a_medida(request,numero_banios,numero_habitaciones):
    obj=casa_a_medida(1000,1000,numero_banios,numero_habitaciones)
    doc_externo= get_template('pagina_casa_a_medida.html')
    documento=doc_externo.render({"precio_habitacion":obj.get_precio_habitacion,"precio_banio":obj.get_precio_banio,"numero_habitaciones":obj.get_numero_habitaciones,"numero_banios":obj.get_numero_banios,"precio_final":obj.precio_final})
    return HttpResponse(documento)
